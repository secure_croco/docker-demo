#===========================================================================================================================================
#
# More info on the NTFSSecurity PowerShell module:
# ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
#
# GitHub repository
# https://github.com/raandree/NTFSSecurity
#
# Compiled module on Script Center
# https://gallery.technet.microsoft.com/scriptcenter/1abd77a5-9c0b-4a2b-acef-90dbb2b84e85
#
# Tutorial from Microsoft blog
# https://blogs.technet.microsoft.com/heyscriptingguy/2014/11/22/weekend-scripter-use-powershell-to-get-add-and-remove-ntfs-permissions/
#===========================================================================================================================================

Expand-Archive -Path "C:\PowerShell-Modules\NTFSSecurity.zip" `
               -DestinationPath "C:\Program Files\WindowsPowerShell\Modules\NTFSSecurity"
			   
Remove-Item "C:\PowerShell-Modules" -Recurse
