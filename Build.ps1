#===============================================================================
# - Stop "g5r" (shutdown the container OS).
#
# - Remove "g5r" (delete the image instance).
#===============================================================================
docker stop g5r
docker rm g5r

#===============================================================================
# - Build the content of current directory AKA "."
#
# - Tag the newly built image as "g5"
#   ("g5" is just a random cool name).
#===============================================================================
docker build --tag g5 .

#===============================================================================
# - Run the image "g5" that we just built,
#   and give a new name "g5r" to the running instance of the image.
#
# - Detach the running container, so that it runs in the background
#   and frees up our commandline.
#
# - Publish port 80 to 80. In order words, associate the port 80
#   of the container to the port 80 of the Docker host machine.
#===============================================================================
docker run --name g5r --detach --publish 80:80 g5

#===============================================================================
# - Inspect our running container "g5r" with a format that
#   allows us to find its current IP address.
#===============================================================================
docker inspect --format "{{ .NetworkSettings.Networks.nat.IPAddress }}" g5r

#===============================================================================
# - Open a PowerShell terminal within the runnin container.
#===============================================================================
docker exec -it g5r powershell
