﻿using System;
using System.Web.Http;

namespace DockerDemo.MyAwesomeApi.Controllers
{
    [RoutePrefix("api/echo")]
    public class EchoController : ApiController
    {
        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok($"Echo OK! " +
                $"Also, here's the current time: {DateTime.Now} " +
                $"and a random GUID: {Guid.NewGuid()}");
        }
    }
}